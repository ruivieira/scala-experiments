package ml.smile

import ml.smile.data.Loader
import smile.data.DataFrame

package object examples {
  val creditBiasDF: DataFrame = Loader.csv("../data/credit_bias-train.csv")
}
