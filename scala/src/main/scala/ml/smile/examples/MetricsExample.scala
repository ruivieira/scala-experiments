package ml.smile.examples

import ml.fairness.Metrics
import ml.smile.data.Loader
import smile.classification.RandomForest
import smile.data.formula.Formula

object MetricsExample {
  def main(args: Array[String]): Unit = {
    val df = Loader.csv(Loader.homeRelativePath("data/credit_bias-train.csv").toString)
    val subset = df
      .select("NewCreditCustomer", "Amount", "Interest", "LoanDuration", "Education", "PaidLoan")
      .omitNullRows()
      .factorize("Education", "NewCreditCustomer", "PaidLoan")

    val target = "PaidLoan"
    val protectedAttribute = "Education"

    val formula = Formula.lhs(target)
    val model = RandomForest.fit(formula, subset)

    model.predict(subset)

    val predictions = Metrics.extendDfWithPredictions(subset, model, "predicted")

    val demographicParity = Metrics.demographicParity(predictions, protectedAttribute, target, "predicted")
    println(demographicParity.mkString(","))

    val demographicParityDifference = Metrics.demographicParityDifference(predictions, protectedAttribute, target, "predicted")
    println(demographicParityDifference)

    val demographicParityRatio = Metrics.demographicParityRatio(predictions, protectedAttribute, target, "predicted")
    println(demographicParityRatio)
    
    val equalisedOddsDiff = Metrics.equalisedOddsDifference(predictions, protectedAttribute, target, "predicted")
    println(equalisedOddsDiff)

    val equalisedOddsRatio = Metrics.equalisedOddsRatio(predictions, protectedAttribute, target, "predicted")
    println(equalisedOddsRatio)
  }
}
