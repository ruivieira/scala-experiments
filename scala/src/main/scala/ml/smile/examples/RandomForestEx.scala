package ml.smile.examples

import smile.classification.RandomForest
import smile.data.`type`.{DataTypes, StructField}
import smile.data.formula.Formula
import smile.data.{DataFrame, Tuple}

object RandomForestEx {
  def main(args: Array[String]): Unit = {

    val subset: DataFrame = creditBiasDF.select("Amount", "LoanDuration", "PaidLoan")

    val formula: Formula = Formula.lhs("PaidLoan")
    val a = RandomForest.fit(formula, subset)
    val schema = DataTypes.struct(new StructField("Amount", DataTypes.DoubleType), new StructField("LoanDuration", DataTypes.DoubleType))
    println(a.predict(
      Tuple.of(Array(10000000.0, 1000.0), schema)
    ))
  }
}
