package ml.smile.providers

import ml.smile.SMILEPredictionProvider
import org.kie.kogito.explainability.model._
import smile.classification.KNN
import smile.data.DataFrame

import java.util
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletableFuture.completedFuture
import scala.jdk.CollectionConverters._

// INFO: SMILE KNN PredictionProvider
class KNNProvider(val model: KNN[Array[Double]]) extends SMILEPredictionProvider {


  def this(x: DataFrame, y: DataFrame) {
    this(KNN.fit(x.toArray, y.toArray.map(y => y(0).toInt)))
  }


  override def predictAsync(inputs: util.List[PredictionInput]): CompletableFuture[util.List[PredictionOutput]] = {
    val values = inputs.get(0).getFeatures.asScala.map(feature => (feature.getValue.asNumber(), feature.getName)).toArray
    val x = Array(values.map(d => d._1))
    val names = values.map(d => d._2)

    val outputs = model.predict(x)
      .zip(names)
      .map(r => new Output(r._2, Type.NUMBER, new Value(r._1), 1d)).toList.asJava
    val predictionOutput: PredictionOutput = new PredictionOutput(outputs)
    completedFuture(List(predictionOutput).asJava)
  }
}
