![logo](../../../../../../../docs/felix.png)

# Smile prediction providers

Prediction providers for TrustyAI's explainers.

![diagram](./diagram.png)

## Model types

- Random forest
- Linear Discriminant Analysis
- KNN