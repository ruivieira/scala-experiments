package ml.smile.providers

import ml.smile.SMILEPredictionProvider
import org.kie.kogito.explainability.model._
import smile.classification.RandomForest
import smile.data.DataFrame
import smile.data.formula.Formula

import java.util
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletableFuture.completedFuture
import scala.jdk.CollectionConverters._

// INFO: SMILE random forest PredictionProvider
class RandomForestProvider(val model: RandomForest) extends SMILEPredictionProvider {

  def this(formula: Formula, df: DataFrame) {
    this(RandomForest.fit(formula, df))
  }

  override def predictAsync(inputs: util.List[PredictionInput]): CompletableFuture[util.List[PredictionOutput]] = {
    val values = inputs.get(0).getFeatures.asScala.map(feature => (feature.getValue.asNumber(), feature.getName)).toArray
    val x = Array(values.map(d => d._1))
    val names = values.map(d => d._2)
    val input_df = DataFrame.of(x, names: _*)

    val outputs = model.predict(input_df)
      .zip(model.formula().response().variables().asScala)
      .map(r => new Output(r._2, Type.NUMBER, new Value(r._1), 1d)).toList.asJava
    val predictionOutput: PredictionOutput = new PredictionOutput(outputs)
    completedFuture(List(predictionOutput).asJava)
  }
}
