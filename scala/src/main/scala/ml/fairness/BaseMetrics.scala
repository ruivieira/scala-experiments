package ml.fairness


object BaseMetrics {

  type Disparity = (Array[Int], Array[Int]) => Double

  /**
   * Calculate the fraction of predicted labels matching the desired outcome
   */
  def selectionRate(trueLabels: Array[Int], predictedLabels: Array[Int]): Double = {
    trueLabels.zip(predictedLabels).count(pair => pair._1 == pair._2).toDouble / trueLabels.length.toDouble
  }


}
