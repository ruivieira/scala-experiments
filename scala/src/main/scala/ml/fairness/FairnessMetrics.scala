package ml.fairness

import smile.classification.DataFrameClassifier
import smile.data.{DataFrame, _}

import scala.collection.immutable

class FairnessMetrics[T](val model: DataFrameClassifier, val df: DataFrame) {

  val predictions: Array[Array[Int]] = model.predict(df).map(x => Array(x))
  private val predicted = df.merge(DataFrame.of(predictions, "predicted"))

  def parityLoss(protectedAttribute: String): immutable.Iterable[ParityLoss] = {

    val groups = predicted.groupBy(row => row.getString(protectedAttribute))

    groups.flatMap({ group =>
      val totalObs = group._2.nrows()
      group._2.groupBy(row => row.getString("predicted"))
        .map(outcome => ParityLoss(group._1, outcome._1, outcome._2.nrows().toDouble / totalObs.toDouble))
    })
  }

}

case class ParityLoss(level: String, outcome: String, value: Double)