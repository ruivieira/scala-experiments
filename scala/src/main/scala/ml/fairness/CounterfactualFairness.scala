package ml.fairness

import org.apache.commons.math3.linear.RealMatrix
import smile.data.DataFrame
import smile.data.formula.Formula
import smile.regression.{LinearModel, OLS}

// INFO: Counterfactually fair models
/**
 * Generates a counterfactually fair model.
 */
class CounterfactualFairness(val data: DataFrame) {

/**
 * Initialise a counterfactually fair model with the specified dataset.
 * The dataset is a {@link RealMatrix} and each column represents a variable and each row
 * represents an observation.
 *
 * @param data The initial dataset as a {@link RealMatrix}
 */

  /**
   * Calculate a model by giving the protected and variable indices.
   *
   * @param protectedFeatures An array of indices for the protected variables
   * @param variableFeatures  An array of indices for the non-protected variables
   * @param target      The index of the target
   */
  def calculate(protectedFeatures: Array[String], variableFeatures: Array[String], target: String): LinearModel = {
    val residuals = variableFeatures.map(variable => (variable, calculateEpsilon(protectedFeatures, variable)))
    val regressionData = DataFrame.of(residuals.map(_._2), residuals.map(_._1): _*).merge(data.select(target))

    // predict target from residuals
    val formula = Formula.rhs(target)
    OLS.fit(formula, regressionData)
  }

  /**
   * Calculate epsilon for the protected indices
   *
   * @param protectedFeatures An array of protected indices
   * @param target      The target index
   * @return
   */
  private def calculateEpsilon(protectedFeatures: Array[String], target: String): Array[Double] = {
    val subset = data.select(protectedFeatures :+ target : _*)
    val formula = Formula.rhs(target)
    val regression = OLS.fit(formula, subset)
    regression.residuals()
  }

}
