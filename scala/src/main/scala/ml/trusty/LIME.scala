package ml.trusty

import org.kie.kogito.explainability.local.lime.{LimeConfig, LimeExplainer}
import org.kie.kogito.explainability.model._

import java.util
import java.util.Random
import scala.jdk.CollectionConverters._

class LIME(val model: PredictionProvider, seed: Int = 23, nPerturbations: Int = 1,
           nSamples: Int = 10) {
  private val limeConfig = new LimeConfig().withPerturbationContext(
    new PerturbationContext(seed, new Random(), nPerturbations))
    .withSamples(nSamples)
  val explainer = new LimeExplainer(limeConfig)

  def explain(
               features: List[Feature]
             ): util.Map[String, Saliency] = {
    val input = new PredictionInput(features.asJava)
    val output = model.predictAsync(List(input).asJava).get()
    val prediction = new SimplePrediction(input, output.get(0))
    explainer.explainAsync(prediction, model).get()
  }
}
