package ml.trusty

import org.kie.kogito.explainability.local.counterfactual.{CounterfactualConfig, CounterfactualExplainer, CounterfactualResult, SolverConfigBuilder}
import org.kie.kogito.explainability.model._
import org.kie.kogito.explainability.model.domain.FeatureDomain
import org.optaplanner.core.config.solver.termination.TerminationConfig

import java.util.UUID
import scala.jdk.CollectionConverters._


// INFO: TrustyAI counterfactual adapter
class Counterfactual(
                      val model: PredictionProvider,
                      val steps: Int = 100_000,
                      val threshold: Double = 0.01
                    ) {

  private val terminationConfig = new TerminationConfig().withScoreCalculationCountLimit(steps)
  private val solverConfig = SolverConfigBuilder.builder.withTerminationConfig(terminationConfig).build
  private val counterfactualConfig = new CounterfactualConfig
  counterfactualConfig.withSolverConfig(solverConfig).withGoalThreshold(threshold)
  val explainer = new CounterfactualExplainer(counterfactualConfig)

  def explain(
               goal: List[Output],
               features: List[Feature],
               domains: List[FeatureDomain],
               constraints: List[Boolean],

             ): CounterfactualResult = {
    val dataDomain = new DataDomain(domains.asJava)
    val input = new PredictionInput(features.asJava)
    val output = new PredictionOutput(goal.asJava)
    val domain = new PredictionFeatureDomain(dataDomain.getFeatureDomains)
    val javaConstraints: java.util.List[java.lang.Boolean] = constraints.map(boolean2Boolean).asJava
    val prediction = new CounterfactualPrediction(input, output, domain, javaConstraints, null, UUID.randomUUID, null)
    explainer.explainAsync(prediction, model).get()

  }
}
