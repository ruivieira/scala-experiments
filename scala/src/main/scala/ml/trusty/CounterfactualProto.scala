package ml.trusty

import ml.trusty.prototype.counterfactuals.configurations.DefaultSolverConfigBuilder
import ml.trusty.prototype.counterfactuals.{CounterfactualConfig, CounterfactualExplainerProto}
import org.kie.kogito.explainability.local.counterfactual.CounterfactualResult
import org.kie.kogito.explainability.model._
import org.kie.kogito.explainability.model.domain.FeatureDomain
import org.optaplanner.core.config.solver.termination.TerminationConfig

import java.util.UUID
import scala.jdk.CollectionConverters._


// INFO: TrustyAI prototype counterfactual adapter
class CounterfactualProto(
                           val model: PredictionProvider,
                           val steps: Int = 100_000,
                           val threshold: Double = 0.01
                         ) {

  private val terminationConfig = new TerminationConfig().withScoreCalculationCountLimit(steps)
  // TODO: Do not hardcode
  private val solverConfig = DefaultSolverConfigBuilder.builder.withTerminationConfig(terminationConfig).build
  private val counterfactualConfig = new CounterfactualConfig
  counterfactualConfig.withSolverConfig(solverConfig).withGoalThreshold(threshold)
  val explainer = new CounterfactualExplainerProto(counterfactualConfig)

  def explain(
               goal: List[Output],
               features: List[Feature],
               domains: List[FeatureDomain],
               constraints: List[Boolean],

             ): CounterfactualResult = {
    val dataDomain = new DataDomain(domains.asJava)
    val input = new PredictionInput(features.asJava)
    val output = new PredictionOutput(goal.asJava)
    val domain = new PredictionFeatureDomain(dataDomain.getFeatureDomains)
    val javaConstraints: java.util.List[java.lang.Boolean] = constraints.map(boolean2Boolean).asJava
    val prediction = new CounterfactualPrediction(input, output, domain, javaConstraints, null, UUID.randomUUID, null)
    explainer.explainAsync(prediction, model).get()

  }
}
