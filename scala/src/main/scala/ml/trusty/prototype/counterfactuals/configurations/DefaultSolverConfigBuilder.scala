/*
 * Copyright 2021 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.trusty.prototype.counterfactuals.configurations

import ml.trusty.prototype.counterfactuals.CounterFactualScoreCalculator
import org.kie.kogito.explainability.local.counterfactual.CounterfactualSolution
import org.kie.kogito.explainability.local.counterfactual.entities.{BooleanEntity, CategoricalEntity, DoubleEntity, IntegerEntity}
import org.optaplanner.core.config.localsearch.LocalSearchPhaseConfig
import org.optaplanner.core.config.localsearch.decider.acceptor.LocalSearchAcceptorConfig
import org.optaplanner.core.config.localsearch.decider.forager.LocalSearchForagerConfig
import org.optaplanner.core.config.phase.PhaseConfig
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig
import org.optaplanner.core.config.solver.SolverConfig
import org.optaplanner.core.config.solver.termination.TerminationConfig

import java.util


object DefaultSolverConfigBuilder {

  def builder = new DefaultSolverConfingBuilder()

}

sealed class DefaultSolverConfingBuilder extends CounterfactualSolverConfig() {

  private val DEFAULT_TABU_SIZE = 70
  private val DEFAULT_ACCEPTED_COUNT = 5000
  private var terminationConfig = new TerminationConfig
  private var tabuSize = DEFAULT_TABU_SIZE
  private var acceptedCount = DEFAULT_ACCEPTED_COUNT

  override def build: SolverConfig = {
    val solverConfig = new SolverConfig
    solverConfig.withEntityClasses(classOf[IntegerEntity], classOf[DoubleEntity], classOf[BooleanEntity], classOf[CategoricalEntity])
    solverConfig.setSolutionClass(classOf[CounterfactualSolution])
    val scoreDirectorFactoryConfig = new ScoreDirectorFactoryConfig
    scoreDirectorFactoryConfig.setEasyScoreCalculatorClass(classOf[CounterFactualScoreCalculator])
    solverConfig.setScoreDirectorFactoryConfig(scoreDirectorFactoryConfig)
    solverConfig.setTerminationConfig(terminationConfig)
    val acceptorConfig = new LocalSearchAcceptorConfig
    acceptorConfig.setEntityTabuSize(tabuSize)
    val localSearchForagerConfig = new LocalSearchForagerConfig
    localSearchForagerConfig.setAcceptedCountLimit(acceptedCount)
    val localSearchPhaseConfig = new LocalSearchPhaseConfig
    localSearchPhaseConfig.setAcceptorConfig(acceptorConfig)
    localSearchPhaseConfig.setForagerConfig(localSearchForagerConfig)
    @SuppressWarnings(Array("rawtypes")) val phaseConfigs = new util.ArrayList[(PhaseConfig[Config_]) forSome {type Config_ <: PhaseConfig[Config_]}]
    phaseConfigs.add(localSearchPhaseConfig)
    solverConfig.setPhaseConfigList(phaseConfigs)
    solverConfig
  }

  def withTabuSize(size: Int): DefaultSolverConfingBuilder = {
    this.tabuSize = size
    this
  }

  def withAcceptedCount(count: Int): DefaultSolverConfingBuilder = {
    this.acceptedCount = count
    this
  }

  def withTerminationConfig(terminationConfig: TerminationConfig): DefaultSolverConfingBuilder = {
    this.terminationConfig = terminationConfig
    this
  }
}
