/*
 * Copyright 2021 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// INFO: Experimental CF explainer
package ml.trusty.prototype.counterfactuals

import org.kie.kogito.explainability.local.LocalExplainer
import org.kie.kogito.explainability.local.counterfactual.{CounterfactualResult, CounterfactualSolution}
import org.kie.kogito.explainability.model.{CounterfactualPrediction, Prediction, PredictionInput, PredictionProvider}
import org.optaplanner.core.config.solver.SolverConfig
import org.slf4j.LoggerFactory

import java.time.Duration
import java.util
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{CompletableFuture, ExecutionException, Executor}
import java.util.function.{Consumer, Function}
import java.util.{Objects, UUID}
import scala.jdk.CollectionConverters._


/**
 * Provides exemplar (counterfactual) explanations for a predictive model.
 * This implementation uses the Constraint Solution Problem solver OptaPlanner to search for
 * counterfactuals which minimize a score calculated by {@link CounterFactualScoreCalculator}.
 */
object CounterfactualExplainerProto {
  val assignSolutionId: Consumer[CounterfactualSolution] = (counterfactual: CounterfactualSolution) => counterfactual.setSolutionId(UUID.randomUUID)
  private val logger = LoggerFactory.getLogger(CounterfactualExplainerProto.getClass)
  /**
   * Wrap the provided {@link Consumer< CounterfactualResult >} in a OptaPlanner-accepted
   * {@link Consumer< CounterfactualSolution >}.
   * The consumer is only called when the provided {@link CounterfactualSolution} is valid.
   *
   * @param consumer {@link Consumer< CounterfactualResult >} provided to the explainer for intermediate results
   * @return {@link Consumer< CounterfactualSolution >} as accepted by OptaPlanner
   */
  private def createSolutionConsumer(consumer: Consumer[CounterfactualResult], sequenceId: AtomicLong): Consumer[CounterfactualSolution] = {
    (counterfactualSolution: CounterfactualSolution) => {
      if (counterfactualSolution.getScore.isFeasible) {
        val result = new CounterfactualResult(counterfactualSolution.getEntities, counterfactualSolution.getPredictionOutputs, counterfactualSolution.getScore.isFeasible, counterfactualSolution.getSolutionId, counterfactualSolution.getExecutionId, sequenceId.incrementAndGet)
        consumer.accept(result)
      }
    }
  }
}

class CounterfactualExplainerProto() extends LocalExplainer[CounterfactualResult] {
  var counterfactualConfig = new CounterfactualConfig

  /**
   * Create a new {@link CounterfactualExplainerProto} using OptaPlanner as the underlying engine.
   * The data distribution information (if available) will be used to scale the features during the search.
   * A customizable OptaPlanner solver configuration can be passed using a {@link SolverConfig}.
   * An specific {@link Executor} can also be provided.
   * The score calculation (as performed by {@link CounterFactualScoreCalculator}) will use the goal threshold
   * to if a proposed solution is close enough to the goal to be considered a match. This will only apply
   * to numerical variables. This threshold is a positive ratio of the variable value (e.g. 0.01 of the value)
   * A strict match can be implemented by using a threshold of zero.
   *
   * @param counterfactualConfig An Counterfactual {@link CounterfactualConfig} configuration
   */
  def this(counterfactualConfig: CounterfactualConfig) {
    this()
    this.counterfactualConfig = counterfactualConfig
  }

  def getCounterfactualConfig: CounterfactualConfig = counterfactualConfig



      override def explainAsync(prediction: Prediction, model: PredictionProvider, intermediateResultsConsumer: Consumer[CounterfactualResult]): CompletableFuture[CounterfactualResult] = {
        val sequenceId = new AtomicLong(0)
        val cfPrediction = prediction.asInstanceOf[CounterfactualPrediction]
        val featureDomain = cfPrediction.getDomain
        val constraints: util.List[java.lang.Boolean] = cfPrediction.getConstraints
        val executionId = cfPrediction.getExecutionId
        val maxRunningTimeSeconds = cfPrediction.getMaxRunningTimeSeconds
        val entities = CounterfactualEntityFactory.createEntities(prediction.getInput, featureDomain, constraints, cfPrediction.getDataDistribution)
        val goal = prediction.getOutput.getOutputs
        val initial: Function[UUID, CounterfactualSolution] = (uuid: UUID) => new CounterfactualSolution(entities, model, goal, UUID.randomUUID, executionId, this.counterfactualConfig.getGoalThreshold)
        val cfSolution = CompletableFuture.supplyAsync(() => {

            val solverConfig = this.counterfactualConfig.getSolverConfig
            if (Objects.nonNull(maxRunningTimeSeconds)) solverConfig.withTerminationSpentLimit(Duration.ofSeconds(maxRunningTimeSeconds))
            try {
              val solverManager = this.counterfactualConfig.getSolverManagerFactory.apply(solverConfig)
              try {

                val solverJob = solverManager.solveAndListen(
                  executionId,
                  initial,
                  CounterfactualExplainerProto.assignSolutionId.andThen(CounterfactualExplainerProto.createSolutionConsumer(intermediateResultsConsumer, sequenceId)),
                  null)

                try {// Wait until the solving ends
                  solverJob.getFinalBestSolution
                } catch {
                  case e: ExecutionException =>
                    CounterfactualExplainerProto.logger.error("Solving failed: {}", e.getMessage)
                    throw new IllegalStateException("Prediction returned an error", e)
                  case e: InterruptedException =>
                    Thread.currentThread.interrupt()
                    throw new IllegalStateException("Solving failed (Thread interrupted)", e)
                }
              } finally if (solverManager != null) solverManager.close()
            }
        }, this.counterfactualConfig.getExecutor)
        val cfOutputs = cfSolution.thenCompose((s: CounterfactualSolution) => {
          val features = s.getEntities.asScala.map(entity => entity.asFeature()).asJava
          model.predictAsync(util.List.of(new PredictionInput(features)))
        })
        CompletableFuture.allOf(cfOutputs, cfSolution).thenApply((v: Void) => {
            val solution = cfSolution.join
            new CounterfactualResult(solution.getEntities, cfOutputs.join, solution.getScore.isFeasible, UUID.randomUUID, solution.getExecutionId, sequenceId.incrementAndGet)
        })
      }
}
