/*
 * Copyright 2021 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.trusty.prototype.counterfactuals

import ml.trusty.prototype.counterfactuals.configurations.DefaultSolverConfigBuilder
import org.kie.kogito.explainability.local.counterfactual.CounterfactualSolution
import org.optaplanner.core.api.solver.SolverManager
import org.optaplanner.core.config.solver.{SolverConfig, SolverManagerConfig}

import java.util.UUID
import java.util.concurrent.{Executor, ForkJoinPool}
import java.util.function.Function


/**
 * Counterfactual explainer configuration parameters.
 */
object CounterfactualConfig {
  private val DEFAULT_GOAL_THRESHOLD = 0.01
}

class CounterfactualConfig() {
  private var executor: Executor = ForkJoinPool.commonPool
  private var solverConfig = DefaultSolverConfigBuilder.builder.build
  private var goalThreshold = CounterfactualConfig.DEFAULT_GOAL_THRESHOLD
  private var solverManagerFactory: Function[SolverConfig, SolverManager[CounterfactualSolution, UUID]] =
    solverConfig => SolverManager.create(solverConfig, new SolverManagerConfig())


  def getSolverManagerFactory: Function[SolverConfig, SolverManager[CounterfactualSolution, UUID]] = solverManagerFactory

  def withSolverManagerFactory(solverManagerFactory: Function[SolverConfig, SolverManager[CounterfactualSolution, UUID]]): CounterfactualConfig = {
    this.solverManagerFactory = solverManagerFactory
    this
  }

  def getExecutor: Executor = executor

  def withExecutor(executor: Executor): CounterfactualConfig = {
    this.executor = executor
    this
  }

  def withGoalThreshold(threshold: Double): CounterfactualConfig = {
    this.goalThreshold = threshold
    this
  }

  def getSolverConfig: SolverConfig = solverConfig

  def withSolverConfig(solverConfig: SolverConfig): CounterfactualConfig = {
    this.solverConfig = solverConfig
    this
  }

  def getGoalThreshold: Double = goalThreshold
}
