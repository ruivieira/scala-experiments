/*
 * Copyright 2021 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.trusty.prototype.counterfactuals

import org.kie.kogito.explainability.local.counterfactual.entities._
import org.kie.kogito.explainability.local.counterfactual.entities.fixed.{FixedBooleanEntity, FixedCategoricalEntity, FixedDoubleEntity, FixedIntegerEntity}
import org.kie.kogito.explainability.model._
import org.kie.kogito.explainability.model.domain.FeatureDomain

import java.util
import java.util.Optional
import scala.jdk.CollectionConverters._

object CounterfactualEntityFactory {
  def from(feature: Feature, isConstrained: Boolean, featureDomain: FeatureDomain): CounterfactualEntity = CounterfactualEntityFactory.from(feature, isConstrained, featureDomain, null)

  def from(feature: Feature, isConstrained: Boolean, featureDomain: FeatureDomain, featureDistribution: FeatureDistribution): CounterfactualEntity = {
    validateFeature(feature)
    val entity: CounterfactualEntity = feature.getType match {
      case Type.NUMBER => if (feature.getValue.getUnderlyingObject.isInstanceOf[Double]) {
        if (isConstrained) {
          FixedDoubleEntity.from(feature)
        } else {
          DoubleEntity.from(feature, featureDomain.getLowerBound, featureDomain.getUpperBound, featureDistribution, isConstrained)
        }
      } else if (feature.getValue.getUnderlyingObject.isInstanceOf[Integer]) {
        if (isConstrained) {
          FixedIntegerEntity.from(feature)
        } else {
          IntegerEntity.from(feature, featureDomain.getLowerBound.intValue, featureDomain.getUpperBound.intValue, featureDistribution, isConstrained)
        }
      } else {
        throw new IllegalArgumentException("Unsupported feature type: " + feature.getType)
      }
      case Type.BOOLEAN => if (isConstrained) {
        FixedBooleanEntity.from(feature)
      } else {
        BooleanEntity.from(feature, isConstrained)
      }
      case Type.CATEGORICAL =>
        if (isConstrained) {
          FixedCategoricalEntity.from(feature)
        } else {
          CategoricalEntity.from(feature, featureDomain.getCategories, isConstrained)
        }
      case _ => throw new IllegalArgumentException("Unsupported feature type: " + feature.getType)
    }
    entity
  }

  /**
   * Validation of features for counterfactual entity construction
   *
   * @param feature {@link Feature} to be validated
   */
  def validateFeature(feature: Feature): Unit = {
    val `type` = feature.getType
    val `object` = feature.getValue.getUnderlyingObject
    if (`type` eq Type.NUMBER) if (`object` == null) throw new IllegalArgumentException("Null numeric features are not supported in counterfactuals")
  }

  def createEntities(predictionInput: PredictionInput,
                     featureDomain: PredictionFeatureDomain,
                     constraints: util.List[java.lang.Boolean],
                     dataDistribution: DataDistribution): util.List[CounterfactualEntity] = {
    val domains = featureDomain.getFeatureDomains
    predictionInput.getFeatures.asScala.indices.map((featureIndex: Int) => {
      val feature = predictionInput.getFeatures.get(featureIndex)
      val isConstrained = constraints.get(featureIndex)
      val domain = domains.get(featureIndex)
      val featureDistribution = Optional.ofNullable(dataDistribution).map((dd: DataDistribution) => Optional.ofNullable(dd.asFeatureDistributions.get(featureIndex)))
      // TODO: Do not hardcode feature distributions
      CounterfactualEntityFactory.from(feature, isConstrained, domain, null)
    }).asJava
  }
}

class CounterfactualEntityFactory private() {
}