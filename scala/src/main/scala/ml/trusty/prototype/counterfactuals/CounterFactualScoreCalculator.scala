/*
 * Copyright 2021 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.trusty.prototype.counterfactuals

import org.kie.kogito.explainability.Config
import org.kie.kogito.explainability.local.counterfactual.CounterfactualSolution
import org.kie.kogito.explainability.local.counterfactual.entities.CounterfactualEntity
import org.kie.kogito.explainability.model.{Output, PredictionInput, PredictionOutput, Type}
import org.optaplanner.core.api.score.buildin.bendablebigdecimal.BendableBigDecimalScore
import org.optaplanner.core.api.score.calculator.EasyScoreCalculator
import org.slf4j.LoggerFactory

import java.math.BigDecimal
import java.util
import java.util.Objects
import java.util.concurrent.{ExecutionException, TimeoutException}
import scala.jdk.CollectionConverters._


/**
 * Counterfactual score calculator.
 * The score is implementabled as a {@link BendableBigDecimalScore} with two hard levels and one soft level.
 * The primary hard level penalizes solutions which do not meet the required outcome.
 * The second hard level penalizes solutions which change constrained {@link CounterfactualEntity}.
 * The soft level penalizes solutions according to their distance from the original prediction inputs.
 */
object CounterFactualScoreCalculator {
  private val logger = LoggerFactory.getLogger(classOf[CounterFactualScoreCalculator])

  @throws[IllegalArgumentException]
  def outputDistance(prediction: Output, goal: Output): Double = outputDistance(prediction, goal, 0.0)

  @throws[IllegalArgumentException]
  def outputDistance(prediction: Output, goal: Output, threshold: Double): Double = {
    val predictionType = prediction.getType
    val goalType = goal.getType
    if (predictionType ne goalType) {
      val message = String.format("Features must have the same type. Feature '%s', has type '%s' and '%s'", prediction.getName, predictionType.toString, goalType.toString)
      logger.error(message)
      throw new IllegalArgumentException(message)
    }
    if (prediction.getType eq Type.NUMBER) {
      val predictionValue = prediction.getValue.asNumber
      val goalValue = goal.getValue.asNumber
      val difference = Math.abs(predictionValue - goalValue)
      // If any of the values is zero use the difference instead of change
      // If neither of the values is zero use the change rate
      var distance = .0
      if (java.lang.Double.isNaN(predictionValue) || java.lang.Double.isNaN(goalValue)) {
        val message = String.format("Unsupported NaN or NULL for numeric feature '%s'", prediction.getName)
        logger.error(message)
        throw new IllegalArgumentException(message)
      }
      if (predictionValue == 0 || goalValue == 0) distance = difference
      else distance = difference / Math.max(predictionValue, goalValue)
      if (distance < threshold) 0d
      else distance
    }
    else if ((prediction.getType eq Type.CATEGORICAL) || (prediction.getType eq Type.BOOLEAN) || (prediction.getType eq Type.TEXT)) {
      val goalValueObject = goal.getValue.getUnderlyingObject
      val predictionValueObject = prediction.getValue.getUnderlyingObject
      if (Objects.equals(goalValueObject, predictionValueObject)) 0.0
      else 1.0
    }
    else {
      val message = String.format("Feature '%s' has unsupported type '%s'", prediction.getName, predictionType.toString)
      logger.error(message)
      throw new IllegalArgumentException(message)
    }
  }
}

class CounterFactualScoreCalculator extends EasyScoreCalculator[CounterfactualSolution, BendableBigDecimalScore] {
  /**
   * Calculates the counterfactual score for each proposed solution.
   * This method assumes that each model used as {@link org.kie.kogito.explainability.model.PredictionProvider} is
   * consistent, in the sense that for repeated operations, the size of the returned collection of
   * {@link PredictionOutput} is the same, if the size of {@link PredictionInput} doesn't change.
   *
   * @param solution Proposed solution
   * @return A {@link BendableBigDecimalScore} with three "hard" levels and one "soft" level
   */
  override def calculateScore(solution: CounterfactualSolution): BendableBigDecimalScore = {
    var primaryHardScore = 0.0
    var secondaryHardScore = 0
    var tertiaryHardScore = 0
    var secondarySoftscore = 0
    val builder = new StringBuilder
    // Calculate similarities between original inputs and proposed inputs
    var inputSimilarities = 0.0
    val numberOfEntities = solution.getEntities.size

    for (entity: CounterfactualEntity <- solution.getEntities.asScala) {
      val entitySimilarity = entity.similarity
      inputSimilarities += entitySimilarity / numberOfEntities.toDouble
      val f = entity.asFeature
      builder.append(String.format("%s=%s (d:%f)", f.getName, f.getValue.getUnderlyingObject, entitySimilarity))
      if (entity.isChanged) {
        secondarySoftscore -= 1
        if (entity.isConstrained) secondaryHardScore -= 1
      }
    }
    // Calculate Gower distance from the similarities
    val primarySoftScore = -Math.sqrt(1.0 - inputSimilarities)
    CounterFactualScoreCalculator.logger.debug("Current solution: {}", builder)
    val input = solution.getEntities.asScala.map(_.asFeature).asJava
    val predictionInput = new PredictionInput(input)
    val inputs = util.List.of(predictionInput)
    val predictionAsync = solution.getModel.predictAsync(inputs)
    val goal = solution.getGoal
    try {
      val predictions = predictionAsync.get(Config.INSTANCE.getAsyncTimeout, Config.INSTANCE.getAsyncTimeUnit)
      solution.setPredictionOutputs(predictions)
      var outputDistance = 0.0
      for (predictionOutput:PredictionOutput <- predictions.asScala) {
        val outputs = predictionOutput.getOutputs
        if (goal.size != outputs.size) throw new IllegalArgumentException("Prediction size must be equal to goal size")
        val numberOutputs = outputs.size
        for (i <- 0 until numberOutputs) {
          val output = outputs.get(i)
          val goalOutput = goal.get(i)
          val d = CounterFactualScoreCalculator.outputDistance(output, goalOutput, solution.getGoalThreshold)
          outputDistance += d * d
          if (output.getScore < goalOutput.getScore) tertiaryHardScore -= 1
        }
        primaryHardScore -= Math.sqrt(outputDistance)
        CounterFactualScoreCalculator.logger.debug("Distance penalty: {}", primaryHardScore)
        CounterFactualScoreCalculator.logger.debug("Changed constraints penalty: {}", secondaryHardScore)
        CounterFactualScoreCalculator.logger.debug("Confidence threshold penalty: {}", tertiaryHardScore)
      }
    } catch {
      case e: ExecutionException =>
        CounterFactualScoreCalculator.logger.error("Prediction returned an error {}", e.getMessage)
      case e: InterruptedException =>
        CounterFactualScoreCalculator.logger.error("Interrupted while waiting for prediction {}", e.getMessage)
        Thread.currentThread.interrupt()
      case e: TimeoutException =>
        CounterFactualScoreCalculator.logger.error("Timed out while waiting for prediction")
    }
    CounterFactualScoreCalculator.logger.debug("Feature distance: {}", -Math.abs(primarySoftScore))
    BendableBigDecimalScore.of(Array[BigDecimal](BigDecimal.valueOf(primaryHardScore), BigDecimal.valueOf(secondaryHardScore), BigDecimal.valueOf(tertiaryHardScore)), Array[BigDecimal](BigDecimal.valueOf(-Math.abs(primarySoftScore)), BigDecimal.valueOf(secondarySoftscore)))
  }
}
