package ml.trusty.prototype.counterfactuals.configurations

import org.optaplanner.core.config.solver.SolverConfig

trait CounterfactualSolverConfig {
  def build: SolverConfig
}
