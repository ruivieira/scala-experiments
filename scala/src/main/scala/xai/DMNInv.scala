package xai

import org.kie.dmn.api.core.DMNRuntime
import org.kie.kogito.dmn.{DMNKogito, DmnDecisionModel}
import org.kie.kogito.explainability.model.{Feature, FeatureFactory, PredictionInput}

import java.io.InputStreamReader
import java.util
import scala.jdk.CollectionConverters._

object  DMNInv {

  private def getTestInput = {
    val client = new util.HashMap[String, Any]
    client.put("Age", 43)
    client.put("Salary", 100)
    client.put("Existing payments", 100)
    val loan = new util.HashMap[String, Any]
    loan.put("Duration", 15)
    loan.put("Installment", 100)
    val contextVariables = new util.HashMap[String, Any]
    contextVariables.put("Client", client)
    contextVariables.put("Loan", loan)
    contextVariables.put("God", "No")
    contextVariables.put("Bribe", 0.0)
    val features = new util.ArrayList[Feature]
    features.add(FeatureFactory.newCompositeFeature("context", contextVariables))
    new PredictionInput(features)
  }
  def main(args: Array[String]): Unit = {

    val rs = getClass().getClassLoader().getResourceAsStream("LoanEligibility.dmn")
    val dmnRuntime: DMNRuntime = DMNKogito.createGenericDMNRuntime(new InputStreamReader(rs))

    val FRAUD_NS = "https://github.com/kiegroup/kogito-examples/dmn-quarkus-listener-example"
    val FRAUD_NAME = "LoanEligibility"
    val decisionModel = new DmnDecisionModel(dmnRuntime, FRAUD_NS, FRAUD_NAME)
    val model = new DecisionModelWrapper(decisionModel, List("Judgement").asJava)
    val result = model.predictAsync(List(getTestInput).asJava).get()
    println(result.get(0).getOutputs)
  }
}
