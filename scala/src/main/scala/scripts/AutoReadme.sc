// INFO: Automatically generates this README.md

import $file.Common

import java.io.{BufferedWriter, FileWriter}
import java.nio.file.Paths
import scala.io.Source

val home = System.getProperty("user.home")
val path = Seq("Sync", "code", "scala", "scala-experiments")
val root = Paths.get(home, path: _*)
val tag = "// INFO: "
val infos = Common.walkTree(root.toFile)
  .filterNot(_.toString.contains("/target/"))
  .filter(f => f.getName.endsWith(".scala") || f.getName.endsWith(".sc")).map(f => {
  val info = Source.fromFile(f).getLines.find(_.startsWith(tag))
  (f, info)
}).filter(_._2.isDefined)
  .map(d => {
    s"- [`${d._1.getName.substring(0, d._1.getName.lastIndexOf('.'))}`](${root.relativize(d._1.toPath).toString}), ${d._2.get.drop(tag.length)}"
  }).mkString("\n")

val template =
  s"""
     |![felix](docs/felix.png)
     |
     |# Scala experiments
     |[![pipeline status](https://gitlab.com/ruivieira/scala-experiments/badges/master/pipeline.svg)](https://gitlab.com/ruivieira/scala-experiments/-/commits/master) ![scala-2.13](docs/scala-2.13.svg)
     |
     |Experimenting with Scala.
     |
     |## Projects
     |
     |${infos}
     |
     |---
     |<sup>Rui Vieira 2022. Logo made with 🅮 images</sup>
     |""".stripMargin

val file = Paths.get(home, path :+ "README.md": _*).toFile
val bw = new BufferedWriter(new FileWriter(file))
bw.write(template)
bw.close()