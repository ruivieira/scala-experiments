import $ivy.`com.github.javaparser:javaparser-symbol-solver-core:3.24.0`
import $ivy.`dev.ruivieira::scala-experiments:0.1.0-SNAPSHOT`
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.expr.BinaryExpr
import com.github.javaparser.ast.stmt.IfStmt
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.github.javaparser.utils.SourceRoot
import parsers.GenericParser

import java.nio.file.{Path, Paths}
val root: Path = Paths.get(System.getProperty("user.home"), "Sync", "code", "rh", "trusty", "kogito-apps", "explainability", "explainability-core").resolve("src/main/java")
val parser = GenericParser(root)