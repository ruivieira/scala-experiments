import org.kie.kogito.explainability.model.{Feature, Output, PredictionInput, PredictionOutput, PredictionProvider, Type, Value}

import scala.jdk.CollectionConverters._
import java.util
import java.util.concurrent.CompletableFuture.supplyAsync

object Models {
  def getSumThresholdModel(center: Double, epsilon: Double): PredictionProvider = {
    (inputs: util.List[PredictionInput]) => supplyAsync(() => {
      def foo() = {
        val predictionOutputs: util.List[PredictionOutput] = new util.LinkedList[PredictionOutput]

        for (predictionInput <- inputs.asScala) {
          val features: util.List[Feature] = predictionInput.getFeatures
          var result: Double = 0
          for (i <- 0 until features.size) {
            result += features.get(i).getValue.asNumber
          }
          val inside: Boolean = result >= center - epsilon && result <= center + epsilon
          val predictionOutput: PredictionOutput = new PredictionOutput(List(new Output("inside", Type.BOOLEAN, new Value(inside), 1.0 - Math.abs(result - center))).asJava)
          predictionOutputs.add(predictionOutput)
        }
        predictionOutputs
      }

      foo()
    })
  }
}
