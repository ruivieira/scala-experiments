package ml.smile

import ml.smile.data.Loader
import smile.data.DataFrame

package object providers {
  val df: DataFrame = Loader.csv("../data/credit_bias-train.csv")


  val amount = 100000000.0
  val duration = 100.0
  val steps = 300L
  val DEFAULT_GOAL_THRESHOLD = 0.01

}
