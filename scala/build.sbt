ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "dev.ruivieira"
ThisBuild / scalaVersion := "2.13.6"

lazy val root = (project in file("."))
  .settings(
    name := "scala-experiments"
  )

libraryDependencies += "org.kie.kogito" % "explainability" % "1.15.0.Final"
libraryDependencies += "org.kie.kogito" % "explainability-core" % "1.15.0.Final"
libraryDependencies += "org.kie.kogito" % "kogito-dmn" % "1.15.0.Final"
libraryDependencies += "com.github.javaparser" % "javaparser-symbol-solver-core" % "3.24.0"
libraryDependencies += "com.github.haifengl" %% "smile-scala" % "2.6.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.10"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.10" % "test"
libraryDependencies += "io.github.cibotech" %% "evilplot" % "0.8.1"
libraryDependencies += "org.bytedeco" % "openblas" % "0.3.17-1.5.6"
libraryDependencies += "org.bytedeco" % "openblas-platform" % "0.3.17-1.5.6"
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6.1"

// OptaPlanner
libraryDependencies += "org.optaplanner" % "optaplanner-core" % "8.15.0.Final"
